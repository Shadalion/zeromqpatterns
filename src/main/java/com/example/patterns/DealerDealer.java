package com.example.patterns;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class DealerDealer {
    public static void main(String[] args) {
        try(ZContext ctx = new ZContext()) {
            new RsThread(ctx, 5555).start();
            //Когда инициируем 2 клиента к 1 узлу, ответы не маршрутизируются и отправляются рандомно, т.е. не факт,
            // что если отправили с одной ноды, то и ответ придет на эту же
            new RqThread(ctx).start();
            new RqThread(ctx).start();
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            System.out.println("FINISH");
        }
    }

    static class RqThread extends Thread {
        private ZContext ctx;

        public RqThread(ZContext ctx) {
            this.ctx = ctx;
        }

        @Override
        public void run(){
            ZMQ.Socket rq = ctx.createSocket(SocketType.DEALER);
            //Requests will round robin among these nodes
            rq.connect("tcp://localhost:5555");
            rq.connect("tcp://localhost:5556");

            for(int i = 0; i < 5; i++) {
                //Add empty frame, rep start find message from empty
                rq.send("Message number: " + i + Thread.currentThread().getName());
            }
            for(int i = 0; i < 5; i++) {
                //After send all messages, wait for reply
                System.out.println("Current thread" + Thread.currentThread().getName() + "  " + rq.recvStr());
            }

        }
    }

    static class RsThread extends Thread {
        private ZContext ctx;
        private int port;

        public RsThread(ZContext ctx, int port) {
            super("THREAD " + port);
            this.ctx = ctx;
            this.port = port;
        }

        @Override
        public void run(){
            ZMQ.Socket rq = ctx.createSocket(SocketType.DEALER);
            rq.bind("tcp://localhost:" + port);
            //Find first empty frame and then receive content
            while(true) {
                String data = rq.recvStr();
                rq.send(Thread.currentThread().getName() + " " + data);
            }
        }
    }
}
