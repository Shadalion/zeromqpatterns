package com.example.patterns;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class ReqRouter {
    public static void main(String[] args) {
        try (ZContext ctx = new ZContext()) {
            new RsThread(ctx, 5555).start();
            new RsThread(ctx, 5556).start();
            new RqThread(ctx).start();
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            System.out.println("FINISH");
        }
    }

    static class RqThread extends Thread {
        private ZContext ctx;

        public RqThread(ZContext ctx) {
            this.ctx = ctx;
        }

        @Override
        public void run() {
            ZMQ.Socket rq = ctx.createSocket(SocketType.REQ);
            //Requests will round robin among these nodes
            rq.connect("tcp://localhost:5555");
            rq.connect("tcp://localhost:5556");

            for(int i = 0; i < 5; i++) {
                //add identity to identify request
                rq.setIdentity(("IDENTITY " + i).getBytes());
                rq.send("Message number: " + i);
                System.out.println(rq.recvStr());
            }
        }
    }

    static class RsThread extends Thread {
        private ZContext ctx;
        private int port;

        public RsThread(ZContext ctx, int port) {
            super("THREAD " + port);
            this.ctx = ctx;
            this.port = port;
        }

        @Override
        public void run() {
            ZMQ.Socket rq = ctx.createSocket(SocketType.ROUTER);
            rq.bind("tcp://localhost:" + port);
            //Find first empty frame and then receive content
            while (true) {
                String identity = rq.recvStr();
                rq.recvStr(); // recv empty frame
                String data = rq.recvStr();
                rq.sendMore(identity);//Send identity to find appropriate connection
                rq.sendMore("");
                rq.send(Thread.currentThread().getName() + " " + data);
            }
        }
    }
}
