package com.example.patterns;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class RqRs {
    public static void main(String[] args) {
        try(ZContext ctx = new ZContext()) {
            new RsThread(ctx, 5555).start();
            new RsThread(ctx, 5556).start();
            new RqThread(ctx).start();
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            System.out.println("FINISH");
        }
    }

    static class RqThread extends Thread {
        private ZContext ctx;

        public RqThread(ZContext ctx) {
            this.ctx = ctx;
        }

        @Override
        public void run() {
            ZMQ.Socket rq = ctx.createSocket(SocketType.REQ);
            //Requests will round robin among these nodes
            rq.connect("tcp://localhost:5555");
            rq.connect("tcp://localhost:5556");

            for (int i = 0; i < 5; i++) {
                //Strictly send, then recv

                //Req automatically add empty frame at start
                rq.send("Message number: " + i);
                System.out.println("Receive: " + rq.recvStr());
            }
        }
    }

    static class RsThread extends Thread {
        private ZContext ctx;
        private int port;

        public RsThread(ZContext ctx, int port) {
            super("THREAD " + port);
            this.ctx = ctx;
            this.port = port;
        }

        @Override
        public void run() {
            ZMQ.Socket rq = ctx.createSocket(SocketType.REP);
            rq.bind("tcp://localhost:" + port);
            //Find first empty frame and then receive content
            rq.send(Thread.currentThread().getName() + " " + rq.recvStr());
        }
    }
}
